# Example of flock command

**flock** - manage locks from shell scripts

Check mylock file for an example.

- [Adquiriendo lock exclusivo desde shell script](https://www.librebyte.net/sistemas-operativos/adquiriendo-lock-exclusivo-desde-shell-script/)
- [Video ES](https://youtu.be/q8Wb4K5-wkc)